<?php
//Esse header serve para funcionar o crossorigin no localhost
//header('Access-Control-Allow-Origin: *');
//Novo header para permitir post
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

//função que trasnforma o utf8 para rodar no navegador
function utf8ize($mixed) {
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = utf8ize($value);
        }
    } else if (is_string ($mixed)) {
        return utf8_encode($mixed);
    }
    return $mixed;
}

//Aqui começa a mágica
//Conexão com o banco de dados
$conecta = mysql_connect("localhost", "root", "root") or print (mysql_error());
mysql_select_db("testes", $conecta) or print(mysql_error());

//SQL para consulta
$sql = "SELECT * FROM noticias";

//Parsear os resultados e transfomar em json
$result = mysql_query($sql, $conecta);
$linha = mysql_fetch_assoc($result);
$resposta = array();
do {
  array_push($resposta,$linha);
}while($linha = mysql_fetch_assoc($result));
$resposta = utf8ize($resposta);

//imprimindo o json
echo json_encode($resposta);

?>
