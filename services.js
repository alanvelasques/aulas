//Trabalhando com interceptors e rootScope
angular.module('starter.services', [])
.config(function($httpProvider){
  $httpProvider.interceptors.push(function($q, $rootScope) {
    return {
      'response': function(response) {
        console.log('interceptado logado:'+$rootScope.logado);
        return response;
      }
    };
  });
})